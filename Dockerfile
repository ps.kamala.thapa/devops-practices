FROM openjdk:8-jdk-alpine3.9
ARG commit_id
ENV var=$commit_id
RUN  mkdir /u01 &&  addgroup -S scott && adduser -S  scott -G  scott  && chown -R scott:scott /u01
COPY ["target/assignment-$commit_id.jar","entrypoint.sh", "/u01/"] 
USER scott
COPY ["entrypoint.sh", "/u01/"] 
WORKDIR /u01
EXPOSE 8090
ENTRYPOINT ["/u01/entrypoint.sh"]
